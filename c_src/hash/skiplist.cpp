/*
Skipt List

1.Create:
1.1 initial the head element for empty content.
1.2 define the head length N and the head list is L0 -> L1 -> L2 -> L3
1.3.traversal the link list and judge if insert a higher level node.

2.Search:
2.1 search from the top level.
2.2 if serach-key > current-level key: from the pre-key download level.
2.3 if key < current level key: from the cur-key download level
2.4 record the level head and pre-key relation to RECORD table L1: l1_pre_key, L2: l2_pre_key...
2.5 until the final level  
2.6 if cannot find the key: return the pre-key.
2.7 if founde the key: return the cur-key.

3.Insert:

3.1 call the search interface and find the pre key
3.2 create a new key-value object and insert to current level link list.
3.3 goto higer level link list and randomly insert a node and append the RECORD table.
3.4 insert the higher level link node according by RECORD table.
3.5 update cnt table.

4.Delete:
4.1 call the search interface and find the cur-key.
4.2 if cannot find cur-key in the table, raise error.
4.3 recursion delete the key from top to bottom.
4.4 update cnt table

DSA design

FUNCTION-1  insert
FUNCTION-2  delete


LinkNode:
FIELD-1 key     (INT)
FIELD-2 value   (OBJECT)
FIELD-3 pre     (LinkNode)
FIELD-4 next    (LinkNode)
FIELD-5 up      (LinkNode)
FIELD-6 down    (LinkNode)


1. A double-direction link list.
FIELD-1 head    (LinkNode)  key = 'E'
FIELD-2 tail    (LinkNode)  key = 'E'
FUNCTION-1  insert_head
FUNCTION-2  delete_head
FUNCTION-3  insert_tail
FUNCTION-4  delete_tail

2. Skip List
2.1 table_head    (LinkNode)
2.2 update_table_head

*/

template<K, V> class Node{

public:
    K key;
    V val;

private:

    

}


