#include <iostream>
using namespace std;

template<typename K, typename V> struct BTNode
{
   K key;
   V val;
   int height;
   BTNode * parent;
   BTNode * left;
   BTNode * right;
   bool color;
   std::string getInfo();
   bool isBalance(){
       int l_h, r_h = 0;
       if (left != nullptr){
           l_h = left->height;
       }
       if (right != nullptr){
           r_h = right->height;
       }
        return abs(l_h - r_h) < 2;
   };
};