
#include<math.h>
#include<stdlib.h>


bool isPrime(long n){
    //n<=3时，质数有2和3
    if (n <= 3) {
        return n > 1;
    }
    //当n>3时，质数无法被比它小的数整除
    for(int i = 2; i < n; i++){
        if (n % i == 0) {
            return false;
        }
    }
    return true;
}

long getNextOdd(long odd_n){
  while (true){
    odd_n ++;
    if (isPrime(odd_n)){
      return odd_n;
    }
  }
  return odd_n;
}