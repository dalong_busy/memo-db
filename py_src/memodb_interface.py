

from index.tree import avlTree

engine = avlTree.AVLTree()

while 1:
    print ("memodb >>")
    cmd_str = input()
    cmd = cmd_str.split(' ')[0]
    param_list = cmd_str.split(' ')[1:]
    
    if cmd.lower().strip() == "set":
        if len(param_list) != 2:
            raise NameError(cmd)
        key = param_list[0].strip()
        val = param_list[1].strip()
        engine.insert(key, val)
        print ("insert success!")
    
    elif cmd.lower().strip() == "get":
        
        if len(param_list) != 1:
            raise NameError(cmd)
        key = param_list[0].strip()

        try:
            print(engine.get(key))
        except KeyError:
            print ("key error %s" % key)
            continue
        
    elif cmd.lower().strip() == "rm":
        if len(param_list) != 1:
            raise NameError(cmd)
        key = param_list[0].strip()
        try:
            engine.remove(key)
        except KeyError:
            print ("key error %s" % key)
            continue
        print ("remove success!")
        
        
        
        
        