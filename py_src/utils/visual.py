from graphviz import Digraph

def in_order_recursion(root, re_list):
    if not root:
        return
    
    in_order_recursion(root.left, re_list)
    re_list.append(root)
    in_order_recursion(root.right, re_list)

def visual(root, name='bst'):

    dot = Digraph(name)
    q = [root]
    while q:
        root = q.pop()
        if root.left:
            dot.edge(root.get_info(), root.left.get_info(), color='red', label='left', )
            q.append(root.left)
        if root.right:
            dot.edge(root.get_info(), root.right.get_info(), color='green', label='right', )
            q.append(root.right)
    dot.render('%s.gv' % "bst", view=True)