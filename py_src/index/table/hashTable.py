
#coding:utf-8

DELETED = 0

def mad(k, m, a=3, b=3):

    return (a * k + b) % m

def mid_square(k):

    k **= 2
    bit_list = []
    while k:
        bit_list.append(k % 10)
        k /= 10
    bit_list.reverse()
    m = len(bit_list)
    mid_list = bit_list[int(m / 4): m - int(m / 4)]
    v = 0
    for i in range(len(mid_list)):
        v *= 10
        v += mid_list[i]
    return v

class HashMap(object):

    def __init__(self ):

        self._capcity = 10000
        self._value = [0] * self._capcity
        self._key   = [0] * self._capcity

    def __setitem__(self, k, v):
        """
        插入函数

        1.求解hash值
        2.检查槽位是否被占用
        2.1 如果没被占用，直接插入
        2.2 如果被占用，重新插入的key和当前key相同，更新value
        2.3 如果被占用，重新插入的key和当前key不相同，重新探测新的key
        2.3.1 求解到新的key，判断是否和当前key相同，相同则更新，不相同则插入
        
        """

        idx = self._hash(k)
        if not self._key[idx]:
            # insert
            self._insert(idx, k, v)
        elif self._key[idx] == k:
            # update
            self._update(idx, v)
        else:
            start = idx
            while True:
                if not self._key[idx] or self._key[idx] == k:
                    break
                idx = self._rehash(idx)
                if idx == start:
                    raise Exception('dict is full')
            if self._key[idx] == k:
                self._update(idx, v)
            else:
                self._insert(idx, k, v)

    def _insert(self, idx, k, v):
        """
        insert a k and v to idx index table.

        """
        self._key[idx] = k
        self._value[idx] = v

    def _update(self, idx, v):
        """
        update info.

        """
        self._value[idx] = v
    
    def __getitem__(self, k):
        """
        获取元素
        1.求解hash值
        2.检查槽位是否被占用，未占用则不存在该key
        2.1 如果当前槽位的值和所取的k一样，那么直接取值
        2.2 如果前槽位的值和所取的k不一样，重新探测
        2.2.1 探测直至与k的值相同为止
        
        key 不存在的情况：
            1）不存在该槽位
            2）探测一圈则表示不存在该key
            3）探测到空槽位则表示不存在该key

        """
        
        idx = self._hash(k)
        if not self._key[idx]:
            raise KeyError(k)
        else:
            if self._key[idx] == k:
                return self._value[idx]
            else:
                start = idx
                while True:
                    if not self._key[idx] or self._key[idx] == k:
                        break
                    idx = self._rehash(idx)
                    if idx == start:
                        raise KeyError(k)
                
                if self._key[idx] == k:
                    return self._value[idx]
                else:
                    raise KeyError(k)

    def __contains__(self, key):
        """

        """
        psss

    def __delitem__(self, ):
        

    def _hash(self, k):

        return  mid_square(k)
    
    def _rehash(self, idx):

        return idx + 1


def test():
    d = HashMap()
    d[10] = 'a'
    d[20] = 'c'
    d[42] = 'b'
    d[42] = '234243'
    d[32] = 'b'
    d[223] = 'b'
    d[32] = 'b'


    print d[32]