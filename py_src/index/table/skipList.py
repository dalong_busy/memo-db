"""
Skip List

1.Create:
1.1 initial the head element for empty content.
1.2 define the head length N and the head array is [L0, L1, L2, L3]
1.3.traversal the link list and judge if insert a higher level node.

2.Search:
2.1 search from the top level.
2.2 if serach-key > current-level key: from the pre-key download level.
2.3 if key < current level key: from the cur-key download level
2.4 record the level head and pre-key relation to RECORD table L1: l1_pre_key, L2: l2_pre_key...
2.5 until the final level  
2.6 if cannot find the key: return the pre-key.
2.7 if founde the key: return the cur-key.

3.Insert:

3.1 call the search interface and find the pre key
3.2 create a new key-value object and insert to current level link list.
3.3 goto higer level link list and randomly insert a node and append the RECORD table.
3.4 insert the higher level link node according by RECORD table.
3.5 update cnt table.

4.Delete:
4.1 call the search interface and find the cur-key.
4.2 if cannot find cur-key in the table, raise error.
4.3 recursion delete the key from top to bottom.
4.4 update cnt table

DSA design

FUNCTION-1  insert
FUNCTION-2  delete


LinkNode:
FIELD-1 key     (INT)
FIELD-2 value   (OBJECT)
FIELD-3 pre     (LinkNode)
FIELD-4 next    (LinkNode)
FIELD-5 up      (LinkNode)
FIELD-6 down    (LinkNode)


1. A double-direction link list.
FIELD-1 head    (LinkNode)  key = 'E'
FIELD-2 tail    (LinkNode)  key = 'E'
FUNCTION-1  insert_head
FUNCTION-2  delete_head
FUNCTION-3  insert_tail
FUNCTION-4  delete_tail

2. Skip List
2.1 table_head    (LinkNode)
2.2 update_table_head

"""

import random

class Node(object):
    """
    List Node
    """
    def __init__(self, val):
        """
        The Consturctor.

        """
        self.val = val
        self.up = None
        self.down = None
        self.next = None
        self.prev = None


class SkipList:
    """
    Skip List.

    """
    def __init__(self,):
        """
        The Constructor.

        """
        self.up_prob = 0.5
        self.head_list = [Node(float('-inf'))]
        self.tail_list = [Node(float('+inf'))]
        self.level = 1
    
    def search(self, num):
        """

        """

        head = self.find_node(num)

        if head.val == num:
            print "find the val!"
        
        if head is self.head:
            raise KeyError("cannot find the data in range!")
        
        return head
    
    def find_node(self, num):
        """
        Find the pre node.
        """
        head = self.head_list[-1]
        while True
            while head.next.val == float('-inf') and head.next.val <= num:
                head = head.next
                # lowest node
                if not head.down:
                    break
                head = head.down
        # if cannot find the node, return the node of last less than the num 
        return head

    def insert_overflow(self, p_node, num):
         
        if random.random() > self.up_prob and p_node.up:
            c_node = p_node.up
            i_node = Node(num)
            # insert node to vertical list
            self.insert_node_v(c_node, i_node)
            # insert node to horizontal list
            self.insert_node_h(c_node, p_node, i_node)
            self.insert_overflow(c_node, num)

    def insert_node_v(self, p_node, i_node):

        p_node_next = p_node.next
        p_node.next = i_node
        i_node.prev = p_node
        i_node.next = p_node_next
        p_node_next.prev = i_node

    def insert_node_h(self, p_node, d_node, i_node):

        self.insert_node_v(p_node, i_node)
        i_node.down = d_node
        d_node.up = i_node

    def insert(self, num):

        # 1.find the last num less than num
        p_node = find_node(num)

        # 2.insert
        i_node = Node(num)
        self.insert_node(p_node, i_node)
        if random.random() > up_prob:
        # 3.judge need to up

        # 3.1 find the up level range node
        # 3.2 insert up level
        # 3.3 recursion judge cur level to overflow
        # 3.4 if current level is max level, add level
        self.insert_overflow(p_node, num)

        # 4.update skip list info cnt, head_list, tail_list
        # 4.1 up and down info
        # 4.2 prev and next info

    def delete_node_h(self, d_node):

        d_node_prev = d_node.prev
        d_node_next = d_node.next
        d_node_prev.next = d_node_next
        d_node_next.prev = d_node_prev
        del d_node

    def delete_node_v(self, p_node):

        if not p_node:
            return 
        self.delete_node_v(p_node.up)
        i_node = p_node.next
        if i_node.down:
            i_node.down.up = None
            i_node.down = None
        self.delete_node_h(p_node, i_node):

    def remove(self, num):
        
        # 1.find the last num.
        # 1.1 if None raise error.
        d_node = self.search(num)
        
        # 2.delete
        
        
        # 3.judge need to delete upper node.
        # 3.1 recursion delete upper node.
        # 3.3 stop condition is up node is None
        # 3.3 if delete level is None
        self.delete_node_v(d_node)

        # 4.update skip list info cnt, head_list, tail_list
        # 4.1 up and down info
        # 4.2 prev and next info


class Node:
    def __init__(self, val):

        self.val = val
        self.pre = None
        self.next = None


class CircularDeque(object):

    def __init__(self, k):
        """
        Initialize your data structure here. Set the size of the deque to be k.
        :type k: int
        """
        self.head = Node('#')
        self.tail = Node('#')
        self.head.next = self.tail
        self.tail.pre = self.head
        self.cnt = 0
        self.k   = k

    def insertFront(self, value):
        """
        Adds an item at the front of Deque. Return true if the operation is successful.
        :type value: int
        :rtype: bool
        """
        if self.isFull():
            return False

        node = Node(value)

        head_next = self.head.next
        # 1
        self.head.next = node
        # 2
        node.pre = self.head
        # 3
        node.next = head_next
        # 4
        head_next.pre = node
        self.cnt += 1
        return True

    def insertLast(self, value):
        """
        Adds an item at the rear of Deque. Return true if the operation is successful.
        :type value: int
        :rtype: bool
        """
        if self.isFull():
            return False
        tail_pre = self.tail.pre
        node = Node(value)
        # 1
        tail_pre.next = node
        # 2
        node.pre = tail_pre
        # 3
        node.next = self.tail
        # 4
        self.tail.pre = node
        self.cnt += 1
        return True

    def deleteFront(self):
        """
        Deletes an item from the front of Deque. Return true if the operation is successful.
        :rtype: bool
        """

        if self.isEmpty():
            return False

        front = self.head.next
        front_next = front.next
        # 1
        self.head.next = front_next
        # 2
        front_next.pre = self.head
        # 3
        del(front)
        self.cnt -=1
        return True

    def deleteLast(self):
        """
        Deletes an item from the rear of Deque. Return true if the operation is successful.
        :rtype: bool
        """
        if self.isEmpty():
            return False

        last = self.tail.pre
        last_pre = last.pre
        # 1
        last_pre.next = self.tail
        # 2
        self.tail.pre = last_pre
        del(last_pre)
        self.cnt-=1
        return True

    def getFront(self):
        """
        Get the front item from the deque.
        :rtype: int
        """
        if self.isEmpty():
            return -1
        return self.head.next.val

    def getRear(self):
        """
        Get the last item from the deque.
        :rtype: int
        """
        if self.isEmpty():
            return -1
        return self.tail.pre.val

    def isEmpty(self):
        """
        Checks whether the circular deque is empty or not.
        :rtype: bool
        """
        return self.cnt == 0

    def isFull(self):
        """
        Checks whether the circular deque is full or not.
        :rtype: bool
        """
        return self.cnt == self.k

    def printf(self, head):
        if head:
            print head.val
            self.printf(head.next)


class Queue:

    def __init__(self, k):

        self.k = k
        self.deque = CircularDeque(k)
    
    def push_right(self, num):
        self.deque.insertLast(num)  
    
    def push_left(self, num):
        self.deque.insertFront(num) 
    
    def pop_left(self):
        front = self.deque.getFront()
        self.deque.deleteFront()
        return front.val
    
    def pop_right(self,):
        last = self.deque.getRear()
        self.deque.deleteLast()
        return last.val
    

class Stack:

    def __init__(self, k):

        self.k = k 
        self.deque = CircularDeque(k)
    
    def push(self, num):
        self.deque.insertLast(num)
    
    def pop(self, num):
        last = self.deque.getRear()
        self.deque.deleteLast()
        return last.val
