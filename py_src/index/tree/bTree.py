#coding:utf-8
"""
Btree 

m 个关键码 - m阶B树 - m + 1个分支 (m >= 2)

外部节点深度统一相等h

叶子节点统一为 h - 1

[m/2, m] 称为 m/2, m树

m = 5  3, 5树
m = 18 9, 18树


2-3树，最简单的B树 分支树为2，3

2-4树，是红黑树的基础 分支树为2，3，4



"""
class Node:
    
    def __init__(self, keys=[]):
        
        self.parent = None
        self.keys = keys
        self.childs = []
        self.key_cap = 5
        self.key_cap_low = ceil(self.key_cap / 2)
    
    def __str__(self,):
        
        keys_str = "current keys: %s" % (str(self.keys))
        childs_str = " childs: %s" % str([str(node.keys) for node in self.childs if node])
        return  keys_str + childs_str
    
    def is_full(self,):
        return len(self.keys) > self.key_cap
    
    def is_full_low(self,):
        return len(self.keys) < self.key_cap_low
    
    def is_rented(self,):
        return len(self.keys) >= self.key_cap_low
    
    def is_over(self,):
        return len(self.keys) > self.key_cap
    
    def is_leaf(self,):
        
        return all([not child for child in self.childs])
    
    
class BTree:

    def __init__(self,):
        
        self.root = None
        self._hot = None
        self.tree_str = ''
    
    def search(self, key):
        
        node = self._search(self.root, key)
        if node :
            return node
        else:
            raise KeyError("no key in tree!")
    
    def _search(self, root, key):
        
        # 搜索到叶子结点
        if not root:
            return None
        self._hot = root
        # 命中当前节点
        if key in root.keys:
            #print "cache the node in keys:%s" % map(str, root.keys)
            return root
        # 继续下一层搜索（把下一层级缓存调用）
        else:
            #print "continue find in next level!"
            idx, node = self.find_range(key, root.keys, root.childs)
            return self._search(node, key)
    
    def find_range(self, key, keys, childs):

        m = len(childs)
        n = len(keys)
        for i in range(m):
            if i - 1 < 0:
                left = float('-inf')
                right = keys[i]
            elif i > n - 1:
                left = keys[i - 1]
                right = float('+inf')
            else:   
                left = keys[i - 1]
                right = keys[i]

            if left < key < right:
                return i, childs[i]
    
    def find_loc_in_parent(self, key, parent):
        
        idx, node = find_range(self, key, parent.keys, parent.childs)
        
        if node:
            return -1
        else:
            return idx
    
    def insert(self, key):
        
        v = self._search(self.root, key)
        if v:
            return False
        
        if self._hot:
            loc = self.find_key_loc(self._hot.keys, key)
            #print "%s find the location:%s for keys:%s" % (key, loc, self._hot.keys)
            self._hot.keys.insert(loc, key)
            self._hot.childs.insert(loc + 1, None)
        else:
            self.root = Node([])
            self.root.keys.append(key)
            self.root.childs.append(None)
            self.root.childs.append(None)
            self._hot = self.root
        
        self.overflow(self._hot)
        
    def find_key_loc(self, nums, key):
        
        if not nums:
            nums = []
        
        m = len(nums)
        for i in range(m):
            if key > nums[i]:
                continue
            else:
                return i
        return m if nums else 0
    
    def overflow(self, root):
        
        if not root:
            return
        
        if not root.is_full():
            return
        
        print "%s is full " % root
        # 求中位数位置
        mid_idx = (len(root.keys) - 1) / 2
        mid_num = root.keys[mid_idx]
        #print "%s it will be splited the index :%s " % (root, mid_idx)
        
        # 判断当前是为root节点
        if not root.parent:
            #print "current %s parent is None, so we need creat a new node!" % root
            parent = Node([mid_num])
            key_loc = 0
            
            # 创建left_node ,right node
            left_node = Node(root.keys[:mid_idx]) 
            left_node.childs = root.childs[:mid_idx + 1]
            left_node.parent = parent 
            right_node = Node(root.keys[mid_idx + 1:])
            right_node.childs = root.childs[mid_idx + 1:]
            right_node.parent = parent 
            
            # key_loc -> left_node  key_loc + 1 -> right_node 节点重定义
            parent.childs.append(left_node)
            parent.childs.append(right_node)
            
            self.root = parent
        else:
            # 找到keys合适的位置i插入
            parent = root.parent
            key_loc = self.find_key_loc(parent.keys, mid_num)
            #print "we find a location from parent: %s at loc: %s " % (root, key_loc)
            parent.keys.insert(key_loc, mid_num)
            # 创建left_node ,right node
            left_node = Node(root.keys[:mid_idx]) 
            left_node.parent = parent
            right_node = Node(root.keys[mid_idx + 1:])
            right_node.parent = parent 
            
            # key_loc -> left_node  key_loc + 1 -> right_node 节点重定义
            parent.childs[key_loc] = left_node
            parent.childs.insert(key_loc + 1, right_node)
        

        print "parent is %s, left is %s, right is %s" % (parent, left_node, right_node)
        
        #overflow parent
        self.overflow(parent)
    
    def remove(self, key):
        
        
        v = self.search(key)
        if not v:
            return False
        
        loc = self.find_key_loc(v.keys, key)
        # 是叶子节点
        if not v.is_leaf():
            # 找到右节点
            u = v.childs[loc + 1]
            # 找到最接近u的右节点
            while u.is_leaf():
                u = u.childs[0]
            v.keys[loc] = u.keys[0]
        
            # 重定义
            v = u
            loc = 0
        v.keys.remove(key)
        v.childs.remove(v.childs[0])
        
        # 判断是否下溢
        self.underflow(v)
        
        return True
    
    def underflow(self, root):
        
        if not root:
            return
        if not root.is_full_low():
            return
        parent = root.parent
        parent_idx = find_loc_in_parent(root.keys[0], parent)
        n_left_node = None
        n_right_node = None
        
        # 左顾右盼
        if parent_idx - 1 >= 0:
            n_left_node = parent.childs[parent_idx - 1]
        if parent_idx + 1 < len(parent.childs):
            n_right_node = parent.childs[parent_idx + 1]
        retend_node = None
        
        if n_left_node and n_left_node.is_rented():
            retend_node = n_left_node
            # 如果借用的是左侧节点，向右侧旋转
            self._rent_r_rotate(root, retend_node, parent, parent_idx)
        
        elif n_right_node and n_right_node.is_rented():
            retend_node = n_right_node
            # 如果借用的是右侧节点，向左侧旋转
            self._rent_l_rotate(root, retend_node, parent, parent_idx)
        else:
            # 借用父节点
            retend_node = n_left_node or n_right_node
            if retend_node:
                self._combine_parent(root, retend_node, parent, parent_idx)
        
    
        self.underflow()
    
    def _rent_r_rotate(root, retend_node, parent, parent_idx):
        
        # 1. key: parent - > root
        parent_split_val = parent.keys[parent_idx]
        root.keys.append(parent_split_val)
        
        # 2. key: neibor -> parent
        n_key = retend_node.keys[0]
        parent.keys[parent_idx] = n_key
        retend_node.keys.remove(n_key)
        
        #3. child: neibor -> root
        n_child = retend_node.childs[0]
        root.childs.append(n_child)
        retend_node.childs.remove(n_child)
        
        #4.
        n_child.parent = root
        
    def _rent_l_rotate(root, retend_node, parent, parent_idx):
        
        # 1. key: parent - > root
        parent_split_val = parent.keys[parent_idx]
        root.keys.insert(0, parent_split_val)
        
        # 2. key: neibor -> parent
        n_key = retend_node.keys[-1]
        parent.keys[parent_idx] = n_key
        retend_node.keys.remove(n_key)
        
        #3. child: neibor -> root
        n_child = retend_node.childs[-1]
        root.childs.insert(0, n_child)
        retend_node.childs.remove(n_child)
        
        #4.
        n_child.parent = root
        
    def _combine_parent(root, retend_node, parent, parent_idx):
        
        # 1.合并节点
        n_node_keys = retend_node.keys
        parent_split_val = parent.keys[parent_idx]
        new_keys = []
        new_keys_childs = []
        if n_node_keys[0] < parent_split_val:
            new_keys = retend_node.keys + [parent.keys[parent_idx]] + root.keys
            new_childs = retend_node.childs + [parent.childs[parent_idx]] + root.childs
        else:
            new_keys = root.keys + [parent.keys[parent_idx]] + retend_node.keys
            new_childs = root.childs + [parent.childs[parent_idx]] + retend_node.childs 
        
        new_node = Node(new_keys)
        new_node.childs = new_childs
        for child in new_node.childs:
            child.parent = new_node
        
        # 2.删除父节点信息
        root.remove(parent.keys, parent_split_val)
        root.remove(parent.childs, parent.childs[parent_idx])
        parent.childs[parent_idx + 1] = new_node
    
    def display(self,):
        
        self._display(self.root, 0)
        print self.tree_str
    
    def _display(self, root, pre_num):
        
        if not root:
            return 
        for child in root.childs:
            self._display(child, pre_num + 1)
        self.tree_str += ('\t ' * pre_num + str(root.keys) + '-\n')        

def test():
    b = BTree()
    b.insert(5)
    b.insert(15)
    b.insert(20)


    b.insert(1)
    b.insert(2)
    b.insert(4)

    b.insert(7)
    b.insert(8)
    b.insert(9)

    b.insert(16)
    b.insert(17)
    b.insert(18)

    b.insert(27)
    b.insert(28)
    b.insert(29)

