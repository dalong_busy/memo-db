#coding:utf-8

import random
import time

class Node:
    """
    Binary Tree node.

    """
    
    def __init__(self, key, val):
        self.key = key
        self.val = val
        self.height = 1
        self.parent = None
        self.left = None
        self.right = None
        # 0 is red, 1 is black
        self.color = 1
    
    def get_info(self):
        
        return "k-%s v-%s h-%s b-%s" % (self.key, self.val, self.height, self.is_balance())

    def is_balance(self, ):
        
        l_h, r_h = 0, 0
        if self.left:
            l_h = self.left.height
        if self.right:
            r_h = self.right.height
        return int(abs(l_h - r_h) < 2)


class BinaryTree(object):
    
    def __int__(self):
        pass

    def _succ(self, node):
        """
        获取左侧后继节点
        :param node:
        :return:
        """
        node = node.right
        succ_node = node
        while node:
            succ_node = node
            node = node.left
        return succ_node

    def _release_node(self, node):
        """
        释放节点内存
        :param node:
        :return:
        """
        # release memory
        del node.val
        del node

    def is_root(self, root):
        """
        是否为根节点
        :param root:
        :return:
        """
        return not (root.parent)

    def _is_left(self, root):
        """
        当前节点是否为左节点
        :param root:
        :return:
        """
        return (not self.is_root(root)) and root and root.parent.left == root

    def _is_right(self, root):
        """
        当前节点是否为右节点
        :param root:
        :return:
        """
        return (not self.is_root(root)) and root and root.parent.right == root

    def _from_parent_to(self, root):
        
        return self.root if self.is_root(root) else (root.left if self._is_left(root) else root.right)

    def _taller_child(self, root):
        
        assert(root)
        taller_node = None
        left_node = root.left
        right_node = root.right
        
        if not right_node:
            taller_node = left_node
        elif not left_node:
            taller_node = right_node
        else:
            if left_node.height >= right_node.height:
                taller_node = left_node
            else:
                taller_node = right_node
        return taller_node


class BST(BinaryTree):
    
    def __init__(self):
        
        self.root = None
        # case1: founded return parent
        # case2: not founded return last key
        self.hot = None
        self.size = 0
        self.height = 0
    
    def _init_tree(self, node):
        """
        初始化树
        :param node:
        :return:
        """
        self.root = node
        self.hot = self.root
        self._insc_tree()

    def _update_height(self, root):
        
        """
        更新当前节点高度
        :param root:
        :return:
        """
    
        struct = lambda x: x.height if x else 0
        root.height = 1 + max(struct(root.left), struct(root.right))

    def _update_v_above(self, root):
        """
        递归更新v以上节点高度
        :param root:
        :return:
        """
        while root:
            self._update_height(root)
            root = root.parent

    def _insc_tree(self,):
        """
        增加节点
        :return:
        """
        self.size += 1
        self._update_v_above(self.hot)

    def _desc_tree(self,):
        """
        删除节点
        :return:
        """
        self.size -= 1
        self._update_v_above(self.hot)
    
    def _swap(self, node1, node2):
        """
        交换节点信息
        :param node1:
        :param node2:
        :return:
        """
        node1.key, node2.key = node2.key, node1.key
        node1.val, node2.val = node2.val, node1.val
    
    def _replace(self, node1, node2):
        """
        用node1覆盖node2
        :param node1:
        :param node2:
        :return:
        """
        node1 = node2
        return node1

    def _remove_at(self, node):
        """
        删除node
        :param node:
        :return:
        """
        # will remove node
        r_node = node
        # replace node
        succ_node = None
        if not node.left:
            succ_node = self._replace(node, node.right)
        elif not node.right:
            succ_node = self._replace(node, node.left)
        else:
            # 1.swap
            # 1.1 get succesor
            succ_node = self._succ(node)
            # 1.2 swap remove node and succesor.
            self._swap(node, succ_node)
            # 1.3 remember will remove node.
            r_node = succ_node
            # 2.replace
            # 2.1 replace succesor and it's right.
            succ_node = self._replace(succ_node, succ_node.right)
        self._hot = r_node.parent
        # update child info.
        if self._is_left(r_node):
            self._hot.left = succ_node
        elif self._is_right(r_node):
            self._hot.right = succ_node
        # is root node
        else:
            self.root = succ_node
        # update parent info.
        if succ_node:
            succ_node.parent = self._hot
        self._release_node(r_node)
        return succ_node
    
    def _search(self, root, _key):
        """
        二分查找接口
        :param root:
        :param _key:
        :return:
        """
        if not root or root.key == _key:
            return root
        self.hot = root
        return self._search(root.left if _key < root.key else root.right, _key)

    def insert(self, i_key, i_val):
        """
        插入数据
        :param i_key:
        :param i_val:
        :return:
        """
        if not self.root:
            iNode = Node(i_key, i_val)
            self._init_tree(iNode)
            return
    
        node = self._search(self.root, i_key)
        # case1: node is null, insert node
        if node:
            node.val = i_val
            return True
        # case2: node is not null, update val
        iNode = Node(i_key, i_val)
        if i_key < self.hot.key:
            self.hot.left = iNode
        else:
            self.hot.right = iNode
        iNode.parent = self.hot
    
        self._insc_tree()
    
        return True

    def remove(self, r_key):
        """
        删除节点
        :param r_key:
        :return:
        """
        node = self._search(self.root, r_key)
        if not node:
            raise KeyError(r_key)

        r_node = self._remove_at(node)
        self._desc_tree()
        return r_node

    def get(self, key):
        """
        获取key的关联信息
        :param key:
        :return:
        """
        node = self._search(self.root, key)
        if not node:
            raise KeyError(key)
        return node.val


class AVLTree(BST):
    
    def insert(self, i_key, i_val):
    
        super(AVLTree, self,).insert(i_key, i_val)
        
        # rebalance
        self._i_banlance()
    
    def remove(self, r_key):
        
        super(AVLTree, self,).remove(r_key)
        
        # rebalance
        self._r_banlance()
    
    def _r_banlance(self,):
    
        g = self.hot
        while g:
            # unbalanced
            if not g.is_balance():
                #print ("[balance_remove]current %s is unbalanced,  we rotate it!" % g.key)
                p_g = g.parent
                g_is_left = self._is_left(g)
                # rotate
                new_g = self.rotate(self._taller_child(self._taller_child(g)))
                if not new_g:
                    g = g.parent
                    continue
                # reconnect
                if not p_g:
                    self.root = new_g
                else:
                    if g_is_left:
                        p_g.left = new_g
                    else:
                        p_g.right = new_g
                    new_g.parent = p_g
                    self._update_height(new_g)
                    self._update_height(p_g)
                self._update_height(g)
            g = g.parent

    def _i_banlance(self, ):
        """
        重平衡
        :return:
        """
        g = self.hot
        while g:
            # unbalanced
            if not g.is_balance():
                #print ("[balance_insert]current %s is unbalanced,  we rotate it!" % g.key)
                p_g = g.parent
                g_is_left = self._is_left(g)
                # rotate
                new_g = self.rotate(self._taller_child(self._taller_child(g)))
                # reconnect
                if not p_g:
                    self.root = new_g
                else:
                    if g_is_left:
                        p_g.left = new_g
                    else:
                        p_g.right= new_g
                    new_g.parent = p_g
                    self._update_height(new_g)
                    self._update_height(p_g)
                break
            else:
                self._update_height(g)
            g = g.parent
    
    def rotate(self, root):
        """
        Rotate the p node according to struct.

        """
        v = root
        if not v:
            return
        p = v.parent
        if not p:
            return 
        g = p.parent
        if not g:
            return 
        #zig 节点 p 右旋转
        if v == p.left and p == g.left:
            return self.zig_zig(v, p, g)
        #zig zag
        elif v == p.right and p == g.left:
            return self.zig_zag(v, p, g)
        #zag
        elif v == p.right and p == g.right:
            return self.zag_zag(v, p, g)
        # zag zig
        elif v == p.left and p == g.right:
            return self.zag_zig(v, p, g)
    
    def zig_zig(self, v, p, g):
        """
        zag-zag rotate.

        """
        # 节点定义
        t1 = v.left
        t2 = v.right
        t3 = p.right
        t4 = g.right
        #print ("[rotate]current struct is zig struct")
        # 向上连接
        p.parent = g.parent
        return self.connect34(v, p, g, t1, t2, t3, t4)
    
    def zig_zag(self, v, p, g):
        """
        zig-zag rotate.

        """
        t1 = p.left
        t2 = v.left
        t3 = v.right
        t4 = g.right
        #print ("[rotate]current struct is zig zag struct")
        v.parent = g.parent
        return self.connect34(p, v, g, t1, t2, t3, t4)
    
    def zag_zig(self, v, p, g):
        """
        zag-zig rotate.

        """
        
        t1 = g.left
        t2 = v.left
        t3 = v.right
        t4 = p.right
        #print ("[rotate]current struct is zag zig struct")
        v.parent = g.parent
        return self.connect34(g, v, p, t1, t2, t3, t4)
    
    def zag_zag(self, v, p, g):
        """
        zag-zag rotate.

        """
        t1 = g.left
        t2 = p.left
        t3 = v.left
        t4 = v.right
        #print ("[rotate]current struct is zag struct")
        p.parent = g.parent
        return self.connect34(g, p, v, t1, t2, t3, t4)
    
    def connect34(self, g, p, v, t1, t2, t3, t4):
        """
        g: grandparent node
        p: parent node
        v: insert node
        t1: sub-tree 1
        t2: sub-tree 2
        t3: sub-tree 3
        t4: sub-tree 4

        Adjust unbalanced struct to balanced struct.

        """

        #print ("[connect34]g:%s p:%s v:%s" % (g.key, p.key, v.key))
        v.left  = t3
        v.right = t4
        if t1:
            t1.parent = g
        if t2:
            t2.parent = g
        
        g.left = t1
        g.right = t2
        if t3:
            t3.parent = v
        if t4:
            t4.parent = v
        
        p.left = g
        p.right = v
        v.parent = p
        g.parent = p
        
        self._update_height(v)
        self._update_height(g)
        self._update_height(p)
        
        return p
    

def test():

    N = 10000
    random_list = [random.randint(1, 100000000000) for i in range(N)]

    bst = AVLTree()

    s = time.time()
    for i in random_list:
        bst.insert(i, i)
    e = time.time()

    cost_time = e - s
    avg_time = cost_time /  N * 1.0
    print  ("%s node avg insert time %s ms" % (N, avg_time * 1000))

    print  ("==========================================")

    s = time.time()
    for i in random_list:
        bst.get(i)
    e = time.time()

    cost_time = e - s
    avg_time = cost_time /  N * 1.0
    print  ("%s node avg search time %s ms" % (N, avg_time * 1000))

    print  ("==========================================")

    s = time.time()
    for i in random_list:
        bst.remove(i)
    e = time.time()

    cost_time = e - s
    avg_time = cost_time /  N * 1.0
    print ("%s node avg remove time %s ms" % (N, avg_time * 1000))

    # for i in range(1000):
    #     bst.get(i)
    # #print  (e - s)
    # for i in range(1, 1000, 5):
    #     bst.remove(i)
    # bst.insert(4, 'd')
    # bst.insert(10, 'j')
    # bst.insert(8, 'h')
    # bst.insert(9, 'i')
    # bst.insert(3, 'c')
    # bst.insert(1, 'a')
    # bst.insert(7, 'g')
    # bst.insert(6, 'f')
    # bst.insert(5, 'e')
    # bst.insert(2, 'bbbbb')
    # bst.remove(1)
    # bst.remove(2)
    # bst.remove(3)
    # bst.remove(4)
    # bst.remove(5)
    # bst.remove(6)
    # bst.remove(7)
    # bst.remove(8)
    # bst.remove(9)
    # bst.remove(10)
    # bst.remove(7)
    # visual(bst.root)
    # re_list = []
    # in_order_recursion(bst.root, re_list)
    # #print  ([node.key for node in re_list])