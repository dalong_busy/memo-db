class Node:
    """
    Binary Tree node.

    """    
    def __init__(self,key, val):

        self.key = key
        self.val = val
        self.height = 0
        self.parent = None
        self.left = None
        self.right = None


class BinaryTree(object):
    """
    Binary Tree.
    
    """
    def __init__(self):
        pass
    
    def pre_order_recursion(self, root, re_list):
        
        if not root:
            return
        re_list.append(root)
        self.pre_order_recursion(root.left, re_list)
        self.pre_order_recursion(root.right, re_list)
    
    def in_order_recursion(self, root, re_list):
        
        if not root:
            return
        
        self.in_order_recursion(root.left, re_list)
        re_list.append(root)
        self.in_order_recursion(root.right, re_list)

    def post_order_recursion(self, root, re_list):
        
        if not root:
            return
        
        self.post_order_recursion(root.left, re_list)
        self.post_order_recursion(root.right, re_list)
        re_list.append(root)
    
    def pre_order_iteration(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """

        res_list=[]
        # node ,status 0 print 1 visit
        # visit : print node visit left ,right
        stack=[(root, 1)]
        while stack:
            cur_struct = stack.pop()
            cur_node, cur_status = cur_struct
            if not cur_node:
                continue
            # 打印
            if cur_status == 0:
                res_list.append(cur_node.val)
            # 访问
            elif cur_status == 1:
                # 先序列遍历
                # 吐栈的顺序
                # 1.打印根节点
                # 2.访问左侧节点
                # 3.访问右侧节点

                # 访问
                stack.append((cur_node.right, 1))
                # 访问
                stack.append((cur_node.left, 1))
                # 打印
                stack.append((cur_node, 0))
        return res_list

    def in_order_iteration(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        res_list=[]
        # node ,status 0 print 1 visit
        # visit : print node visit left ,right
        stack=[(root,1)]
        while stack:
            cur_struct = stack.pop()
            cur_node, cur_status = cur_struct
            if not cur_node:
                continue
            if cur_status == 0:
                res_list.append(cur_node.val)
            elif cur_status == 1:
                stack.append((1, cur_node.right))
                stack.append((0, cur_node))
                stack.append((1, cur_node.left))
        return res_list

    def post_order_iteration(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        res_list=[]
        # node ,status 0 print 1 visit
        # visit : print node visit left ,right
        stack=[(root, 1)]
        while stack:
            cur_struct = stack.pop()
            cur_node, cur_status = cur_struct
            if not cur_node:
                continue
            if cur_status == 0:
                res_list.append(cur_node.val)
            elif cur_status == 1:
                stack.append((1, cur_node.right))
                stack.append((1, cur_node.left))
                stack.append((0, cur_node))
        return res_list


class BinarySearchTree(BinaryTree):
    """
    Binary Search Tree

    """
    def __init__(self, root):
        """
        The Constructor.
        
        """
        self.root = root
        self.hot = None
        self.height = 1
    
    def insert(self, node):
        """
        Insert a node to bst.
        
        """
        if not self._search(self.root,node.key,self.hot):
            if node.key < self.hot.key:
                self.hot.left = node
            else:
                self.hot.right = node
            
            node.parent = self.hot
                
    def max_height(self,root):
        """
        Tree Max depth.
        
        """
        if not root:
            return 1
        return max(self.max_height(root.left), self.max_height(root.left)) + 1
 
    def _search(self,root, e, hot):
        """
        Search elem in BST.
        
        """
        if (not root) or (root.key==e):
            return root
        self.hot = root
        return self._search((root.left if e < root.key else root.right), e, self.hot)
 
    def get(self, key):
        """
        Get value by key.
        
        """
        node_result=self._search(self.root, key, self.hot)
        if not node_result:
            raise KeyError
        return node_result.val
 
    def update_height(self, root):
        
        struct = lambda x: x.height if x else -1
        origin_height = root.height
        root.height = 1 + max(struct(root.left), struct(root.right))
        if origin_height != root.height:
            print "[update_height] current node %s height from %s to %s" % (root.key, origin_height, root.height)
        
    def update_v_above(self, root):
        
        while root:
            self.update_height(root)
            root = root.parent 

    def taller_child(self, root):
        
        return root.left if root.left else root.right
    
    def is_root(self, root):
        
        return not (root.parent)
    
    def is_left(self, root):
        
        return (not self.is_root(root)) and root and root.parent.left == root
    
    def is_left(self, root):
        
        return (not self.is_root(root)) and root and root.parent.right == root
    
    def from_parent_to(self, root):
        
        return self.root if self.is_root(root) else (root.left if self.is_left(root) else root.right)
        
    def inorder_traversal(self, root):
        """
        Inorder traversal.
        
        """
        if not root:
            return
        #indent.append('-')
        self.inorder_traversal(root.left)
        print 'key: %s' % (root.key)
        self.inorder_traversal(root.right)


class AVLTree(BinarySearchTree):
 
    def insert(self, node):
        
        # 插入节点
        if self._search(self.root, node.key, self.hot):
            return
        if node.key < self.hot.key:
            self.hot.left = node
            print "[avl-insert] insert done!, key(%s) left node key(%s)" % (self.hot.key, node.key)
        else:
            self.hot.right = node
            print "[avl-insert] insert done!, key(%s) right node key(%s)" % (self.hot.key, node.key)
        node.parent = self.hot
        
        
        #更新高度
        # self.update_v_above(node)
        
        # 调整
        # self.balance_insert()
    
    def balance_insert(self,):
        
        crt_node = self.hot
        # 计算节点平衡因子
        while crt_node:
            # 失衡
            if not self.is_balance(crt_node):
                print "[balance_insert]current %s is unbalanced,  we roate it!" % crt_node.key
                # 旋转
                orign_ref = self.from_parent_to(crt_node) 
                print "[balance_insert] before adjust,key is %s" % (orign_ref.key)
                self.root = self.rotate(self.taller_child(self.taller_child(crt_node)))
                #print "[balance_insert] after adjust, new main key is %s" % (adjust_ref.key)
                break
            # 平衡
            else:
                # 更新高度
                print "[balance_insert]current %s is balanced,  we update it's height!" % crt_node.key
                self.update_height(crt_node)
            crt_node = crt_node.parent
        print "[balance_insert]arrived the root node !"
            
    def rotate(self, root):
        """
        Rotate the p node according to struct.
        
        """
        v = root
        p = v.parent
        g = p.parent
        #zig 节点 p 右旋转
        if v == p.left and p == g.left:
            return self.zig_zig(v, p, g)
        #zig zag 
        elif v == p.right and p == g.left:
            return self.zig_zag(v, p, g)
        #zag
        elif v == p.right and p == g.right:
            return self.zag_zag(v, p, g)
        # zag zig
        elif v == p.left and p == g.right:
            return self.zag_zig(v, p, g)

    def zig_zig(self,v, p, g):
        """
        zag-zag rotate.

        """
        # 节点定义
        t1 = v.left
        t2 = v.right
        t3 = p.right
        t4 = p.right
        print "[rotate]current struct is zig struct"
        # 向上连接
        p.parent = g.parent
        return self.conect34(v, p, g, t1, t2, t3, t4)

    def zig_zag(self, v, p, g):
        """
        zig-zag rotate.

        """
        t1 = p.left
        t2 = v.left
        t3 = v.right
        t4 = g.right
        print "[rotate]current struct is zig zag struct"
        v.parent = g.parent
        return self.conect34(p, v, g, t1, t2, t3, t4)

    def zag_zig(self, v, p, g):
        """
        zag-zig rotate.

        """

        t1 = g.left
        t2 = p.left
        t3 = v.left
        t4 = v.right
        print "[rotate]current struct is zag zig struct"
        v.parent = g.parent
        return self.conect34(g, v, p, t1, t2, t3, t4)  
    
    def zag_zag(self, v, p, g):
        """
        zag-zag rotate.

        """
        t1 = g.left
        t2 = v.left
        t3 = v.right
        t4 = p.right
        print "[rotate]current struct is zag struct"
        p.parent = g.parent
        return self.conect34(g, p, v, t1, t2, t3, t4)            

    def conect34(self, g, p, v, t1, t2, t3, t4):
        """
        g: grandparent node
        p: parent node
        v: insert node
        t1: sub-tree 1
        t2: sub-tree 2
        t3: sub-tree 3
        t4: sub-tree 4
        
        Adjust unbalanced struct to balanced struct.
        
        """
        
        #print "grandparent: %s\nparent: %s\ncurrent: %s" % ()
        print "[conect34]g:%s p:%s v:%s" % (g.key, p.key, v.key)
        v.left = t1
        v.right = t2
        if t1:
            t1.parent = v
        if t2:
            t2.parent = v
        
        g.left = t3
        g.right = t4
        if t3:
            t3.parent = g
        if t4:
            t4.parent = g
        
        p.left = v
        p.right = g
        v.parent = p
        g.parent = p
        
        self.update_height(v)
        self.update_height(g)
        self.update_height(p)
        
        return p
    
    def is_balance(self, root):
        """
        Check root node is balanced.
        
        """
        left_height = root.left.height if root.left else 0
        right_height = root.right.height if root.right else 0
        print "[is_balance] current node %s, left_height:%s, right_height:%s" % (root.key, left_height, right_height)
        return abs(left_height - right_height) < 2


class Splay(AVLTree):

    def search(self, key):

        node = self._search(self.root, key, self._hot)
        self.root = self.splay(node if node else self._hot) 

    def splay(self, v,):

        p = v.parent
        g = p.parent

        # 逐层伸展
        while p and g:
            gg = g.parent
            is_left =  True if gg.left == g else False
            new_v = self.roate(v)

            if not gg:
                new_v = None
            else:
                self.attach_as_left(gg, new_v) if is_left else self.attach_as_left(gg, new_v)
                update_height(v)
                update_height(p)
                update_height(g)
        v.parent = None
        return v
    
    def attach_as_left(self, root, child):
        """
        Attach the root left node is child node.

        """
        root.left = child
        child.parent = root

    def attach_as_right(self, root, child):
        """
        Attach the root right node is child node.
        """
        root.right = child
        child.parent = root

    def zig(self, v, p):
        """
        zig rotate.

        """
        self.attach_as_left(p, v.right)
        self.attach_as_right(v, p)
    
    def zag(self, v, p):
        """
        zag rotate.

        """
        self.attach_as_right(p, v.left)
        self.attach_as_left(v, p)

    def zig_zig(self, v, p, g):
        """
        zig-zig rotate: override the parent class.

        """
        # 由下向上重构
        self.attach_as_left(g, p.right)
        self.attach_as_left(p, v.right)
        self.attach_as_right(p, g)
        self.attach_as_right(v, p)
        print "[rotate]current struct is zig struct"
        # 向上连接
        v.parent = g.parent
        return v

    def zag_zag(self, v, p, g):
        """
        zag-zag rotate: override the parent class.

        """
        self.attach_as_left(p, v)
        self.attach_as_left(g, v):
        self.attach_as_right(v, p.left)
        self.attach_as_right(p, g.left)
        print "[rotate]current struct is zag struct"
        v.parent = g.parent
        return v       

    
