#coding:utf-8
import random
import numpy as np
import heapq
import time
class Node:
    """
    k-d tree node info
    
    """
    def __init__(self, is_left):
        
        self.split_index = None
        self.vector = None
        self.left = None
        self.right = None
        self.is_left = is_left
        self.is_leaf = False
        

class KDTree:
    """
    k-d tree.
    
    """
    def __init__(self, matrix, nearest_k=3):
        """
        Constructor.
        
        """
        m = len(matrix)
        self.n = len(matrix[0])
        self.root = Node(False)
        self.build_tree_bfs(matrix, 0, self.root)
        self.nearest_k = nearest_k
        self.heap_list = []
    
    def build_tree_dfs(self, matrix, cur_index, root,):
        """
        Build Tree.
        
        """
        cur_m = len(matrix)
        if cur_m == 0:
            return
        if cur_m == 1:
            root.vector = matrix[0]
            root.is_leaf = True
            return
        matrix.sort(key = lambda x: x[cur_index])
        root.split_index = cur_index
        root.vector = matrix[(cur_m/2)]
        root.left = Node(True)
        root.right = Node(False)
        self.build_tree(matrix[:cur_m/2], (cur_index+1) % self.n, root.left)
        self.build_tree(matrix[cur_m/2:], (cur_index+1) % self.n, root.right)
    
    def traversal(self, root):

        if root:
            
            self.traversal(root.left)
            print "leaf node %s, is_leaf: %s, ,%s, %s" % (root.vector, root.is_leaf, root.is_left, root.split_index)
            self.traversal(root.right)

    def build_tree_bfs(self, matrix, cur_index, root):
        """
        Build Tree.
        """
        cur_info = (matrix, cur_index, root)
        stack = [cur_info]
        
        while stack:
            matrix, cur_index, root = stack.pop()
            cur_m = len(matrix)
            if cur_m == 1:
                root.vector = matrix[0]
                root.is_leaf = True
                #print "leaf node %s, is_leaf: %s %s" % (root.vector, root.is_leaf, root.is_left)
                continue 
            matrix.sort(key = lambda x: x[cur_index])
            root.split_index = cur_index
            root.vector = matrix[(cur_m / 2 )]

            left_data = matrix[:cur_m / 2]
            right_data = matrix[cur_m / 2 + 1:]

            if len(left_data) > 0:
                root.left = Node(True)
                stack.append((left_data, (cur_index+1) % self.n, root.left))
            if len(right_data) > 0:
                root.right = Node(False)
                stack.append((right_data, (cur_index+1) % self.n, root.right))

        print "--------"
        # self.traversal(self.root)
        # import pdb
        # pdb.set_trace()
        
    def dis(self, vector1, vector2):
        """
        Distance mearsure.
        
        """
        
        import math
        dis_sum = 0
        for v1, v2 in zip(vector1, vector2):
            dis_sum += (v1 - v2) ** 2
        return math.sqrt(dis_sum)
    
    def search(self, query):
        """
        Search nearst point.
        
        """
        
        if len(query) != self.n:
            raise Error("dim is not matched")
            
        node_list = []
        crt_root = self.root
        # 初始化
        min_dis = self.dis(query, crt_root.vector)
        near_point = crt_root.vector

        # 二分法求解路径【下沉到叶子结点 】
        while not crt_root.is_leaf:
            node_list.append(crt_root)
            if self.dis(query, crt_root.vector) < min_dis:
                min_dis = self.dis(query, crt_root.vector)
                nn_point = crt_root.vector
            
            # 当前节点的分割纬度
            split_index = crt_root.split_index
            # 在split_index的纬度下 query的值 与 节点的值比较
            if query[split_index] < crt_root.vector[split_index]:
                crt_root = crt_root.left
            else:
                crt_root = crt_root.right
        
        # 栈结构模拟回溯 【回溯到根节点】
        while node_list:
            
            crt_node = node_list.pop()
            if crt_node.is_leaf:
                continue
            split_index = crt_node.split_index
            # 判断是否与父节点的空间相交
            if abs(query[split_index] - crt_node.vector[split_index]) > min_dis:
                continue
            if query[split_index] < crt_node.vector[split_index]:
                crt_node = crt_node.left
            else:
                crt_node = crt_node.right
            if not crt_node:
                continue
            node_list.append(crt_node)
            if self.dis(query, crt_root.vector) < min_dis:
                min_dis = self.dis(query, crt_root.vector)
                nn_point = crt_root.vector
        
        return nn_point, min_dis
    
    def _update_top_list(self, top_list, elem):
        """
        Update nearest lane seq list.

        """
        
        heapq.heappush(top_list, elem)
        if len(top_list) > self.nearest_k:
            heapq.heappop(top_list)
        
        return top_list
        
    def update_nn_table(self, heap_list, query, crt_root):

        if not crt_root.vector:
            return
        crt_dis = self.dis(query, crt_root.vector)
        if (-crt_dis, crt_root.vector) not in heap_list:
            self._update_top_list(heap_list, (-crt_dis, crt_root.vector))

    def get_max_dis_pt(self, heap_list,):

        if heap_list:
            max_dis = -heap_list[0][0]
        else:
            max_dis = 1e10
        
        return max_dis

    def get_peer_node(self, query, crt_node):

        peer_node = None
        s_idx = crt_node.split_index
        if query[s_idx] <= crt_node.vector[s_idx]:
            if crt_node.right:
                peer_node = crt_node.right
        else:
            if crt_node.left:
                peer_node = crt_node.left

        return peer_node

    def search_nn_pts(self, query):
        """
        Search nearst point.
        
        """
        
        if len(query) != self.n:
            raise Error("dim is not matched")
            
        node_list = []
        crt_root = self.root
        # 初始化
        nn_point = crt_root.vector
        min_dis = self.dis(query, crt_root.vector)
        near_point = crt_root.vector
        
        
        

        #（1）【下沉到叶子结点】:二分法求解路径
        # 在kd树中找出半酣目标点query的野节点，从根节点出发，递归地向下访问kd树，
        # 若目标点query当前纬度的坐标小于切分点的坐标，则移动到左子输
        # 否则移动到右子节点，直到移动到叶子结点为止
        while crt_root :
            node_list.append(crt_root)
            if crt_root.is_leaf:
                break
            # 当前节点的分割纬度
            split_index = crt_root.split_index
            # 在split_index的纬度下 query的值 与 节点的值比较
            if query[split_index] < crt_root.vector[split_index]:
                crt_root = crt_root.left
            else:
                crt_root = crt_root.right

        #（2）以此节点为当前最近点
        heap_list = [(-self.dis(query, node_list[-1].vector), node_list[-1].vector)]
        

        #（3）【栈结构模拟回溯】递归的向上回退，在每个节点节点进行如下操作
        # a.如果该节点保存的实例点比当前最近的距离目标更近，则以该点为实例点为『当前最近点』
        # b.当前最近点一定存在于该节点一个子节点对应的区域，检查该子节点父节点的另一个区域是否有更近的点，
        # 具体的，检查另一节点对应的区域是否与以目标为球心，以目标点与『当前最近点』间的距离为半径的超球体相交。
        # 如果相交，递归的进行搜索
        # 如果不相交，向上回退
        while node_list:

            crt_node = node_list.pop()

            if crt_node.is_leaf:
                self.update_nn_table(heap_list, query, crt_node)
                continue
            
            # case a
            max_dis = self.get_max_dis_pt(heap_list)
            crt_dis = self.dis(query, crt_node.vector)
            if crt_dis < max_dis:
                self.update_nn_table(heap_list, query, crt_node)
            
            # case b
            s_idx = crt_node.split_index
            max_dis = self.get_max_dis_pt(heap_list)
            peer_node = self.get_peer_node(query, crt_node)
            if peer_node:
                radius = abs(query[s_idx] - peer_node.vector[s_idx])
                if radius < max_dis:
                    node_list.append(peer_node)
        
        top_list = map(lambda x: (-x[0], x[1]), heap_list)
        top_list.sort(key = lambda x:x[0])

        return top_list

    def search_nn(self, query):

        self.dfs(query, self.root)
        top_list = map(lambda x: (-x[0], x[1]), self.heap_list)
        top_list.sort(key = lambda x:x[0])

        return top_list
    
    def dfs(self, query, crt_root):

        if not crt_root:
            return
        if crt_root.is_leaf:
            self.update_nn_table(self.heap_list, query, crt_root)
            # import pdb
            # pdb.set_trace()
            return


        # 当前节点的分割纬度
        split_index = crt_root.split_index
        # 在split_index的纬度下 query的值 与 节点的值比较
        if query[split_index] < crt_root.vector[split_index]:
            self.dfs(query, crt_root.left)
        else:
            self.dfs(query, crt_root.right)
        

        # case a
        max_dis = self.get_max_dis_pt(self.heap_list)
        crt_dis = self.dis(query, crt_root.vector)
        self.update_nn_table(self.heap_list, query, crt_root)
        
        # case b
        s_idx = crt_root.split_index
        max_dis = self.get_max_dis_pt(self.heap_list)
        peer_root = self.get_peer_node(query, crt_root)
        if peer_root:
            radius = abs(query[s_idx] - peer_root.vector[s_idx])
            if radius < max_dis:
                self.dfs(query, peer_root)


def test():
    tmp = [[random.randint(1,10000), random.randint(1,10000)] for i in range(100000)]
    #tmp = [[i,j] for i, j in zip(range(10), range(10))]
    def dis(vector1, vector2):
        """
        Distance mearsure.

        """

        import math
        dis_sum = 0
        for v1, v2 in zip(vector1, vector2):
            dis_sum += (v1 - v2) ** 2
        return math.sqrt(dis_sum)
    def cacl_nn(tmp):
        nn_point = None
        min_dist = float('+inf')
        for point in tmp:
            if dis(point, [99999.0, 88888.0]) < min_dist:
                nn_point = point
                min_dist = dis(point, [99999.0, 88888.0])
    start_time = time.time()
    kd_tree = KDTree(tmp)
    print kd_tree.search_nn([3, 2])
    end_time = time.time()
    print "my code %s" % (end_time - start_time)

    import scipy.spatial
    start_time = time.time()
    kd_tree = KDTree(tmp)
    mytree = scipy.spatial.cKDTree(tmp)
    dist_ckd, indexes_ckd = mytree.query([3, 2], k=3)
    end_time = time.time()
    print "kdtree code %s" % (end_time - start_time)
    print dist_ckd, indexes_ckd