class Point:
    
    def __init__(self, x, y):
        
        self.x = x
        self.y = y
    
    def __add__(self, p):
        
        Point(self.x + p.x, self.y + p.y)
        

    def __str__(self, ):
        
        return "<point> x:%s, y:%s" % (self.x, self.y)
        

class MinBoxRegion:
    """
    Min Rectangle Box Range.
    
    """
    def __init__(self, left_top, right_bottom):
        """
        The Constructor.

        """
        # left top
        self.lt   = left_top
        # right bottom
        self.rb   = right_bottom
        self.area = self.calc_area(self.lt, self.rb)
    
    def __str__(self,):
        """
        Format output.

        """

        return "MBR(LT:%s RB:%s)" % (self.lt, self.rb)

    def calc_area(self):
        """
        Calc the area of MBR.

        """
        
        return abs((self.rb.x - self.lt.x) * (self.lt.y - self.rb.y))

    def is_intersection(self, mbr):
        """
        Judge is intersectd with mbr.

        """
        
        rec1_lt = self.lt
        rec1_rb = self.rb
        rec2_lt = mbr.lt
        rec2_rb = mbr.rb
        
        return max(rec1_lt.x, rec2_lt.x) <= min(rec1_rb.x, rec2_rb.x) and max(rec1_rb.y, rec2_rb.y) <= min(rec1_lt.y, rec2_lt.y)
    
    def calc_delta_area(self, mbr):
        """
        Calc the increased area after adding mbr.

        """
        
        new_lt   = Point(min(mbr.lt.x, self.lt.x), max(mbr.lt.y, self.lt.y))
        new_rb   = Point(max(mbr.rb.x, self.rb.x), min(mbr.rb.y, self.rb.y))
        new_mbr  = MinBoxRegion(new_lt, new_rb)
        new_area = new_mbr.calc_area()
        
        return abs(new_area - self.calc_area())
    
    def update(self, mbr):
        """
        Update the mbr info after adding mbr.

        """
        
        self.lt = Point(min(mbr.lt.x, self.lt.x), max(mbr.lt.y, self.lt.y))
        self.rb = Point(max(mbr.rb.x, self.rb.x), min(mbr.rb.y, self.rb.y))
    
    def calc_center(self,):
        
        return Point((self.rb.x - self.lt.x) / 2.0, (self.lt.y - self.rb.y) / 2.0)

class Node:
    """
    The node of Rectangle Tree.

    """
    
    def __init__(self, mbr, is_leaf=True):
        """
        The Constructor.

        """
        
        self.mbr     = mbr
        self.data    = None
        self.elems   = []
        self.parent  = None
        self.is_leaf = is_leaf
        self.max_cap = 5
        self.min_cap = ceil(self.max_cap / 2.0)
    
    def is_full(self,):
        """
        Judge is node is full.

        """
        
        return len(self.elems) > self.max_cap
    
    def is_unfulle(self,):
        """
        Judge is node is not full.

        """
        
        return len(self.elems) < self.min_cap


import math

class Rtree:
    """
    Rregion Tree.
    
    """
    def __init__(self,):
        
        self.root = None
        self._hot = None
    
    def search(self, k_mbr):
        """
        Search interface.

        k_mbr: key mbr object.

        """
        
        rel_nodes = []
        self._search(self.root, k_mbr, rel_nodes)
        return rel_nodes
    
    def _search(self, root, k_mbr, rel_nodes):
        
        # 如果是叶子节点，返回相关的element
        if root.is_leaf:
            # 找到叶子结点所有相关的element
            nodes = [node for node in root.elems if node.mbr.is_intersection(k_mbr)]
            rel_nodes.extend(nodes)
            return
    
        self._hot = root
        # 递归搜索
        for node in root.elems:
            # 剪枝
            if node.mbr.is_intersection(k_mbr):
                self._search(node, k_mbr, rel_nodes)
    
    def choose_leaf(self, root, i_mbr):
        
        # 1.如果root是叶子节点
        if root.is_leaf:
            return root
        
        # 2.如果root不是叶子节点, 选择扩张最小的node
        rel_nodes = [node for node in root.elems if node.mbr.is_intersection(i_mbr)]
        rel_nodes.sort(key=lambda x: x.mbr.calc_delta_area(i_mbr))
       

        # 4.递归搜索
        if rel_nodes:
             # 3.如果扩展面积相等，选择小面积的
             if rel_nodes[0].mbr.calc_delta_area(i_mbr) == rel_nodes[0].mbr.calc_delta_area(i_mbr):
                if rel_nodes[0].mbr.calc_area() < rel_nodes[1].mbr.calc_area():
                    return self.choose_leaf(rel_nodes[0])
                else:
                    return self.choose_leaf(rel_nodes[1])
            else:
                return self.choose_leaf(rel_nodes[0])
        
    
    def insert(self, i_node):
        
        # 1.chooseleaf
        node = self.choose_leaf(self.root, i_node.mbr)
        if not node:
            # self.root.elems.append(i_node)
            node = Node()
            node.elems = [i_node]
            self.root.append(node)
        else:
            #2.插入
            node.elems.append(i_node)
        
        #3.更新mbr
        node.update(i_node.mbr)
        
        #4.调整
        self.adjust_tree(node)
    
    def _combine_mbr(self, mbrs):
        
        if not mbrs:
            raise RuntimeError("Non MBR list!")

        new_lt = mbrs[0].lt
        new_rb = mbrs[0].rb
        for crt_mbr in mbrs:
            new_lt  = Point(min(mbr.lt.x, new_lt.x), max(mbr.lt.y, new_lt.y))
            new_rb  = Point(max(mbr.rb.x, new_rb.x), min(mbr.rb.y, new_rb.y))
        new_mbr = MinBoxRegion(new_lt, new_rb)
        
        return new_mbr
        
    def _distance(self, p1, p2):
        
        """
        Calc the distance of p1 and p2.
        
        p1:Point Object
        p2:Point Object
        
        """
        
        return math.sqrt((p1.x - p2.x) ** 2 + (p1.y - p2.y) ** 2)
    
    
    def adjust_tree(self, root):
        """
        Split the space by strategy if node is full.
        
        """
        
        if not root.parent:
            return
        
        if not root.is_full():
            return
        
        # 区域分组
        m = len(root.elems)
        elems  = root.elems
        parent =  root.parent
        # 1.计算elems对种增量最大的pair对
        max_delta_area_tuple = None
        max_delta_area = 0
        for i in range(m):
            for j in range(i + 1, m):
                e1             = elems[i]
                e2             = elems[j]
                e1_area        = e1.mbr.calc_area()
                e2_area        = e2.mbr.calc_area()
                e1_e2_mbr      = self._combine_mbr([e1.mbr, e2.mbr])
                e_e2_area      = e1_e2_mbr.calc_area()
                crt_delta_area = e_e2_area- e1_area - e2_area
                if crt_delta_area > max_:
                    max_delta_area = crt_delta_area
                    max_delta_area_tuple = (e1, e2)
        
        # 2.计算距离elem1 和elem2最近的 elems进行分组
        elem_set_1 = []
        elem_set_2 = []
        e1, e2    = max_delta_area_tuple
        e1_center = e1.calc_center()
        e2_center = e2.calc_center()
        for elem in elems:
            elem_center = elem.mbr.calc_center()
            e1_elem_dis = _distance(e1_center, elem_center)
            e2_elem_dis = _distance(e2_center, elem_center)
            if e1_elem_dis < e2_elem_dis:
                elem_set_1.append(elem)
            else:
                elems.remove(elem)
                elem_set_2.append(elem)
        
        # 3.节点重定义
        # 3.1 创建对象
        new_node = Node(root.is_leaf)
        # 3.2 创建mbr集合
        new_mbrs = [elem.mbr for elem in elem_set_2]
        new_mbr = self._combine_mbr(new_mbrs)
        # 3.3 添加分要素到新组
        new_node.elems = elem_set_2
        # 3.4 引用更新
        new_node.parent = parent
        parent.elems.append(new_node)
        
        # 4.递归
        self.adjust_tree(parent)
        
        
    def delete(self,):
        pass