MAX_LEN = 1000
class SegmentTree:
        
    def __init__(self, array):
        
        self.array  = array
        self.tree_arr = [None] * MAX_LEN
        self._build_tree(0, 0, len(self.array) - 1)
    
    def update(self, idx, val):
        
        self._update_tree(0, 0, len(self.array) - 1, idx, val)
    
    def query(self, L, R):
        
        return self._query_tree(0, 0, len(self.array) - 1, L, R)
    
    def _build_tree(self, node, start, end):
        """
        Build segment tree.
        """
        # 停止条件
        if start == end:
            self.tree_arr[node] = self.array[start]
            print "arrived the leaf node (%s: %s)" % (node, self.array[start])
            return
        else:
            # 计算下标
            mid = int((start + end) / 2)
            left_idx = 2 * node + 1
            right_idx = 2 * node + 2
            #左右递归
            self._build_tree(left_idx, start, mid)
            self._build_tree(right_idx, mid + 1, end)
            # 建树
            self.tree_arr[node] = self.tree_arr[left_idx] + self.tree_arr[right_idx]
            print "backtrace the crt node %s ,left value: %s ,right value %s " % (node, self.tree_arr[left_idx], self.tree_arr[right_idx])

            
    def _update_tree(self, node, start, end, idx, val):
        """
        Update segment tree.
        """
        # 停止条件
        if start == end:
            self.array[start] = val
            self.tree_arr[node] = val
            print "arrived the leaf node (%s: %s)" % (node, self.array[start])
            return
        else:
            # 计算下标
            mid = int((start + end) / 2)
            left_idx = 2 * node + 1
            right_idx = 2 * node + 2
            #左右递归
            if idx >= start and idx <=mid:
                self._update_tree(left_idx, start, mid, idx, val)
            else:
                self._update_tree(right_idx, mid + 1, end, idx, val)
            # update
            self.tree_arr[node] = self.tree_arr[left_idx] + self.tree_arr[right_idx]
            print "backtrace the crt node %s ,left value: %s ,right value %s " % (node, self.tree_arr[left_idx], self.tree_arr[right_idx])

    def _query_tree(self, node, start, end, L, R):
        """
        Query segment tree.
        
        """
        print "s:%s, e:%s" % (start, end)
        # 无交集
        if R < start or L > end:
            return 0
        elif L <= start and R >= end:
            return self.tree_arr[node]
        elif start == end:
            return self.tree_arr[node]
        
        # 计算下标
        mid = int((start + end) / 2)
        left_idx = 2 * node + 1
        right_idx = 2 * node + 2
        
        left_sum = self._query_tree(left_idx, start, mid, L, R)
        right_sum = self._query_tree(right_idx, mid + 1, end, L, R)
        
        return left_sum + right_sum
